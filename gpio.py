import sys

import uhd

usage = """Usage:
    python gpio.py pin_bank pin_number pin_state [motherboard]

    pin_bank - FP0 for B210
    pin_number - Number, 0-7 for B210
    pin_state - 0 or 1 to set the output z or Z to get the input.
    motherboard - Motherboard index, defaults to 0
"""

if len(sys.argv) < 4:
    print(usage)
    sys.exit(1)

pin_bank = sys.argv[1]
pin_number = int(sys.argv[2])
pin_state = sys.argv[3]
motherboard = 0
if len(sys.argv) > 4:
    motherboard = int(sys.argv[4])

assert pin_state in ['0', '1', 'z', 'Z']

device = uhd.usrp.MultiUSRP()

print("*"*80)
print("*"*80)
print(device.get_mboard_name())

def set_gpio_attribute(attribute, bit_value):
    device.set_gpio_attr(pin_bank,
                         attribute,
                         bit_value << pin_number,
                         1 << pin_number,
                         motherboard)

def get_gpio_attribute(attribute):
    return device.get_gpio_attr(pin_bank,
                                attribute,
                                motherboard)

if pin_state in ['0', '1']:
    output = int(pin_state)
    assert output == 0 or output == 1

    print(f"Pin {pin_number} on bank {pin_bank} of motherboard {motherboard}")
    print(f"Setting to {output}")

    set_gpio_attribute("CTRL", 0)
    set_gpio_attribute("OUT", output)
    set_gpio_attribute("DDR", 1)

elif pin_state in ['z', 'Z']:
    set_gpio_attribute("CTRL", 0)
    set_gpio_attribute("DDR", 0)

    print(f"Pin {pin_number} on bank {pin_bank} of motherboard {motherboard}")
    print(f"Getting")

    result = get_gpio_attribute("READBACK")

    print(f"{(result >> pin_number) & 1}")

print("*"*80)
print("*"*80)
