import sys

import uhd

device = uhd.usrp.MultiUSRP()

print("*"*80)
print("*"*80)
print(device.get_mboard_name())
if device.get_mboard_name() != "B210":
    print("This script is for use with B210, bye!")
    sys.exit(2)

pin_bank = "FP0"
motherboard = 0

def set_gpio_attribute(attribute, bit_value):
    device.set_gpio_attr(pin_bank,
                         attribute,
                         bit_value << pin_number,
                         1 << pin_number,
                         motherboard)

def get_gpio_attribute(attribute):
    return device.get_gpio_attr(pin_bank,
                                attribute,
                                motherboard)

while True:
    line = input("Give pin_number, pin_state or 'q' for quit: ")
    if line.strip().lower() == 'q':
        break

    pin_number, pin_state = line.strip().split(",")

    pin_number = int(pin_number)
    if not 0 <= pin_number <= 7:
        print(f"Pin number {pin_number} out of range 0-7, try again")
        continue

    if pin_state not in ['0', '1', 'z', 'Z']:
        print(f"Pin state {pin_state} not in '0', '1', 'z' or 'Z', try again")
        continue

    if pin_state in ['0', '1']:
        output = int(pin_state)
        assert output == 0 or output == 1

        print(f"Pin {pin_number}")
        print(f"Setting to {output}")

        set_gpio_attribute("CTRL", 0)
        set_gpio_attribute("OUT", output)
        set_gpio_attribute("DDR", 1)

    elif pin_state in ['z', 'Z']:
        set_gpio_attribute("CTRL", 0)
        set_gpio_attribute("DDR", 0)

        print(f"Pin {pin_number}")
        print(f"Getting")

        result = get_gpio_attribute("READBACK")

        print(f"{(result >> pin_number) & 1}")
